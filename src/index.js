import React from "react";
import ReactDOM from "react-dom";
import queryString from 'query-string';

class ListOfKeys extends React.Component {
  render() {
    const parsed = queryString.parse(location.search)
    const keys = Object.keys(parsed);

    return (
      <ul>
        {keys.map(k => (<li key={k}>{k}: {parsed[k]}</li>))}
      </ul>
    );
  }
}

var mountNode = document.getElementById('app');
ReactDOM.render(<ListOfKeys />, mountNode);
